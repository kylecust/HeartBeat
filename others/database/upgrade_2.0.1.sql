SET NAMES utf8;

-- base on V-2.0.0


-- 2019-06-24
ALTER TABLE application_instance ADD COLUMN  `request_body`      VARCHAR(1024);
ALTER TABLE application_instance ADD COLUMN  `connect_type`     VARCHAR(255)          DEFAULT 'HTTP';

-- 2020-03-17
ALTER TABLE system_setting ADD COLUMN `proxy_enabled` TINYINT(1)            DEFAULT '0';
ALTER TABLE system_setting ADD COLUMN `proxy_host` VARCHAR(255) ;
ALTER TABLE system_setting ADD COLUMN `proxy_port` INT(11)            DEFAULT 0;

-- 2020-03-18
ALTER TABLE application_instance ADD COLUMN `use_proxy` TINYINT(1)    DEFAULT '0';
