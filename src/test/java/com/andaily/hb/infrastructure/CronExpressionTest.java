package com.andaily.hb.infrastructure;

import org.junit.Test;
import org.quartz.CronExpression;


import java.util.Date;

import static org.junit.Assert.assertNotNull;


/**
 * 2017/1/21
 *
 * @author Shengzhao Li
 */
public class CronExpressionTest {


    @Test
    public void test() throws Exception {

        String exp = "0 0 2 * * ?";
        CronExpression cronExpression = new CronExpression(exp);

        Date date = DateUtils.getDate("2017-01-21 14:26:34", DateUtils.DEFAULT_DATE_TIME_FORMAT);
        final Date timeAfter = cronExpression.getTimeAfter(date);

        assertNotNull(timeAfter);
        System.out.println(DateUtils.toDateTime(timeAfter));


    }

}
