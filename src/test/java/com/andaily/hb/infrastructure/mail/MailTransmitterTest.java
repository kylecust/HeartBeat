package com.andaily.hb.infrastructure.mail;

import com.andaily.hb.ContextTest;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNotNull;


/**
 * @author Shengzhao Li
 */
public class MailTransmitterTest extends ContextTest {


    @Test()
    @Ignore
    public void testSend() throws Exception {

        final String emailAddress = "shengzhao@andaily.com";
        MailTransmitter transmitter = new MailTransmitter("HeartBeat Testing", "I'm HeartBeat, just for Testing send mail... <br/> Please ignore...", emailAddress);
        final MailTransmitResult result = transmitter.transmit();
        assertNotNull(result);
        assertTrue(result.success());

    }

}