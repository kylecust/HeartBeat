package com.andaily.hb;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Just test context
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class HeartBeatApplicationTests {

    @Test
    public void contextLoads() {
    }

}
