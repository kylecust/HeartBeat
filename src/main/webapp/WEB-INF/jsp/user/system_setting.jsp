<%--
 * 
 * @author Shengzhao Li
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fun" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>设置</title>
</head>
<body>
<%--Alert--%>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-success" style="display: none;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div><em class="fui-check-circle"></em>
                <span id="updateSettingOK" style="display: none;">保存设置成功</span>
                <span id="cleanLogOK" style="display: none;">清理监控日志成功, 共清理了 ${param.cleaned_amount} 条数据</span>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="row">
        <div class="col-md-12">
            <h4>设置</h4>
            <form:form commandName="formDto" cssClass="form-horizontal" action="setting.hb">
                <div class="form-group">
                    <label for="allowUserRegister" class="col-sm-2 control-label">允许用户注册</label>

                    <div class="col-sm-8">
                        <form:checkbox path="allowUserRegister" id="allowUserRegister"/>
                        <p class="help-block">
                            是否允许用户注册; 若允许则注册的用户有以下权限: <br/>[<em>Create/Edit
                            instance, Delete instance, Start/Stop monitoring instance</em>]
                        </p>
                        <form:errors path="allowUserRegister" cssClass="text-danger"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">自动清理监控日志</label>

                    <div class="col-sm-8">
                        <form:select path="cleanMonitorLogFrequency" cssClass="form-control">
                            <form:option value="15" label="15天"/>
                            <form:option value="30" label="30天"/>
                            <form:option value="45" label="45天"/>
                            <form:option value="60" label="60天"/>
                            <form:option value="90" label="90天"/>
                        </form:select>
                        <div class="help-block">
                            超出指定天数的监控日志(表<code>monitoring_reminder_log</code>与<code>frequency_monitor_log</code>)系统将自动清理(执行清理时间是凌晨2时),
                            目的是提高日志查询速度; 默认30天.
                            <br/>
                            当前系统的监控日志数量为 <strong>${formDto.monitorLogAmount}</strong>(<code>frequency_monitor_log</code>表),
                            <a class="btn btn-link btn-sm" href="javascript:void(0);"
                               id="executeCleanLog">立即执行清理监控日志</a>
                        </div>
                        <form:errors path="cleanMonitorLogFrequency" cssClass="text-danger"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="proxyEnabled" class="col-sm-2 control-label">启用代理</label>

                    <div class="col-sm-8">
                        <form:checkbox path="proxyEnabled" id="proxyEnabled"/>
                        <p class="help-block">
                            是否启用监控请求使用代理; 若启用请配置代理地址与代理端口
                        </p>
                        <form:errors path="proxyEnabled" cssClass="text-danger"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="proxyHost" class="col-sm-2 control-label">代理地址</label>

                    <div class="col-sm-8">
                        <form:input path="proxyHost" id="proxyHost" maxlength="255" placeholder="127.0.0.1"
                                    cssClass="form-control"/>
                        <p class="help-block">
                            请输入代理IP地址, 如: 127.0.0.1
                        </p>
                        <form:errors path="proxyHost" cssClass="text-danger"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="proxyPort" class="col-sm-2 control-label">代理端口</label>

                    <div class="col-sm-8">
                        <form:input path="proxyPort" id="proxyPort" maxlength="9" placeholder="8081"
                                    cssClass="form-control"/>
                        <p class="help-block">
                            请输入代理端口号, 如: 8081
                        </p>
                        <form:errors path="proxyPort" cssClass="text-danger"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary"><em class="fui-check-circle"></em> 保存
                        </button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>

<form action="execute_clean_log.hb" method="post" onsubmit="return confirm('确认立即执行清理超出天数的监控日志?')"
      id="executeCleanLogForm" class="hidden"></form>

<script>
    $(function () {
        new SystemSetting('${param.alert}');
    });
</script>
</body>
</html>