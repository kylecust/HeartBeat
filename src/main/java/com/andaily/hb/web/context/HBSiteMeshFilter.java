package com.andaily.hb.web.context;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;

/**
 * 2018/2/3
 * <p>
 * Replace decorator.xml
 * <p>
 * Sitemesh
 *
 * @author Shengzhao Li
 */
public class HBSiteMeshFilter extends ConfigurableSiteMeshFilter {


    public HBSiteMeshFilter() {
    }


    @Override
    protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {

        builder.addDecoratorPath("/*", "/WEB-INF/jsp/decorators/main.jsp")

                .addExcludedPath("/static/**")
                .addExcludedPath("/load_addition_monitor_logs.hb*")
                .addExcludedPath("/monitoring/statistics/*.hb*")
                .addExcludedPath("/user/reset_password/*.hb*")
                .addExcludedPath("/user/delete/*.hb*")
                .addExcludedPath("/login.hb*");


    }
}
