package com.andaily.hb.service.impl;

import com.andaily.hb.domain.dto.HBSearchDto;
import com.andaily.hb.domain.dto.IndexAdditionInstanceDto;
import com.andaily.hb.domain.dto.IndexDto;
import com.andaily.hb.domain.dto.MonitoringInstanceDto;
import com.andaily.hb.domain.shared.Application;
import com.andaily.hb.service.IndexService;
import com.andaily.hb.service.operation.search.HBSearchDtoLoader;
import com.andaily.hb.service.operation.IndexAdditionInstanceDtoLoader;
import com.andaily.hb.service.operation.IndexDtoLoader;
import com.andaily.hb.service.operation.MonitoringInstanceDtoLoader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Shengzhao Li
 */
@Service("indexService")
@Transactional(readOnly = true)
public class IndexServiceImpl implements IndexService {


    @Override
    public IndexDto loadIndexDto(IndexDto indexDto) {
        IndexDtoLoader dtoLoader = new IndexDtoLoader(indexDto);
        return dtoLoader.load();
    }

    @Override
    public IndexAdditionInstanceDto loadIndexAdditionInstanceDto(IndexAdditionInstanceDto additionInstanceDto) {
        IndexAdditionInstanceDtoLoader dtoLoader = new IndexAdditionInstanceDtoLoader(additionInstanceDto);
        return dtoLoader.load();
    }

    @Override
    public MonitoringInstanceDto loadMonitoringInstanceDto(String guid) {
        MonitoringInstanceDtoLoader dtoLoader = new MonitoringInstanceDtoLoader(guid);
        return dtoLoader.load();
    }

    @Override
    public HBSearchDto loadHBSearchDto(HBSearchDto searchDto) {
        HBSearchDtoLoader dtoLoader = new HBSearchDtoLoader(searchDto);
        return dtoLoader.load();
    }

    @Override
    public boolean loadAllowUserRegister() {
        return Application.systemSetting().allowUserRegister();
    }
}