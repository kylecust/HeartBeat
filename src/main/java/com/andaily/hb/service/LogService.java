package com.andaily.hb.service;

import com.andaily.hb.domain.dto.log.FrequencyMonitorLogListDto;
import com.andaily.hb.domain.dto.log.ReminderLogListDto;

/**
 * 15-2-13
 *
 * @author Shengzhao Li
 */

public interface LogService {

    FrequencyMonitorLogListDto loadFrequencyMonitorLogListDto(FrequencyMonitorLogListDto listDto);

    ReminderLogListDto loadReminderLogListDto(ReminderLogListDto listDto);

    long executeAutoCleanMonitorLogs();
}