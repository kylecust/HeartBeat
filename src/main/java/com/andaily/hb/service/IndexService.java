package com.andaily.hb.service;

import com.andaily.hb.domain.dto.HBSearchDto;
import com.andaily.hb.domain.dto.IndexAdditionInstanceDto;
import com.andaily.hb.domain.dto.IndexDto;
import com.andaily.hb.domain.dto.MonitoringInstanceDto;

/**
 * @author Shengzhao Li
 */

public interface IndexService {

    IndexDto loadIndexDto(IndexDto indexDto);

    IndexAdditionInstanceDto loadIndexAdditionInstanceDto(IndexAdditionInstanceDto additionInstanceDto);

    MonitoringInstanceDto loadMonitoringInstanceDto(String guid);

    HBSearchDto loadHBSearchDto(HBSearchDto searchDto);

    boolean loadAllowUserRegister();
}