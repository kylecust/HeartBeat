package com.andaily.hb.service.operation.search;

import com.andaily.hb.domain.dto.HBSearchDto;

/**
 * 15-3-13
 *
 * @author Shengzhao Li
 */
public interface HBSearcher {

    HBSearchDto search(HBSearchDto searchDto);
}
