package com.andaily.hb.domain.shared.security;

/**
 * @author Shengzhao Li
 */

public interface SecurityHolder {

    AndailyUserDetails userDetails();

}